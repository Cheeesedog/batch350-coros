-- Connection to MySQL via Terminal: mysql -u root
	-- "-u stands for username"
	-- "root" is the default username for sql.
	-- "-p" stands for password
	-- An empty values is the default password for SQL.

-- List down the databases inside the DBMS.
SHOW DATABASES;

-- Note:
	-- SQL QUERIES is case insensitive, but to easily identify the queries we usually use UPPERCASE
	-- Make sure semi-colons (;) are added at the end of the sql syntax.

-- Create a database
CREATE DATABASE music_db;

-- Note: Naming convention in SQL Database and table uses the snake case.

-- Remove a specific database
DROP DATABASE music_db;

-- Select the database.
USE music_db;

-- Create tables
-- Table columns have the following format:
	-- [column_name] [data_type] [other_options]

-- Create user table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT, 
	username VARCHAR(50) NOT NULL, 
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL, 
	contact_number VARCHAR(50) NOT NULL, 
	email VARCHAR(50) NOT NULL, 
	address VARCHAR(50),
	PRIMARY KEY (id)
); 

-- To show tables under a database
SHOW TABLES;

-- Describing a table allows us to see the table columns, data_types and extra_options.

DESCRIBE users;

-- Create artists table
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- Tables with foreign keys;
-- Applying constraints in a table
-- Syntax
--     CONSTRAINT foreign_key_name
--         FOREIGN KEY (column_name)
--         REFERENCES table_name(id)
--         ON UPDATE ACTION
--         ON DELETE ACTION

-- When to create a constraints (foreign key)?
	-- If the relationship is one-to-one, "primary key" of the parent row is added as "foreign key" of the child row.
	-- If the relationship is one-to-many, "foreign key" column is added in the "many" entity/table.
	-- If the relationship is "many-to-many", linking table is created to contain the "foreign key" for both tables/entities

-- Create albums table

-- DATE and TIME FORMAT in SQL
	-- DATE refers to YYYY-MM-DD
	-- TIME refers to HH:MM:SS
	-- DATETIME refers to YYYY-MM-DD HH:MM:SS

CREATE TABLE albums(
	id INT NOT NUll AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NUll,
	artist_id INT NOT NUll,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create songs table

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_name VARCHAR(50) NOT NULL,
	length TIME NOT NULL,
	genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

--Create playlist table
CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	datetime_created DATETIME NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

-- Create a joining playlists songs table
CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlists_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);