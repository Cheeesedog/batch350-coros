-- Activity

-- Inserting records for users
INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00"), ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00"), ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00"), ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00"), ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- Inserting records for posts
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00"), (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00"), (2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00"), (4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");

-- Get all post with an author id of 1
SELECT * FROM posts WHERE author_id = 1;

-- Get all users email and datetime creation
SELECT email, datetime_created FROM users;

-- Update Hello Earth to Hello to the people of the Earth!
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete user with an email of "johndoe@gmail.com"
DELETE FROM users WHERE email = "johndoe@gmail.com";

-- STRETCH GOALS

-- Insert User
INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("johndoe", "john1234", "John Doe", "09123456789", "john@mail.com", "Quezon City");

-- Insert playlist
INSERT INTO playlists (datetime_created, user_id) VALUES ("2023-09-20 08:00:00", 1);

-- Insert playlist song
INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), (1, 2);